# Terrarm | Provisioners

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Définition
Les provisioners sous Terraform sont des fonctionnalités permettant de configurer et de personnaliser les instances et ressources déployées au-delà de leur simple création. Ils offrent la possibilité d'automatiser des tâches de configuration post-déploiement, telles que l'installation de logiciels, la configuration réseau, ou l'exécution de scripts sur les machines provisionnées. Les provisioners peuvent être utiles pour automatiser des processus d'initialisation ou de configuration spécifiques à une application ou à une infrastructure, offrant ainsi une solution complète pour le déploiement et la configuration de l'infrastructure. Cependant, il est recommandé de limiter l'utilisation des provisioners aux tâches essentielles et non répétables, et de privilégier l'utilisation de solutions de configuration plus robustes comme Ansible ou Puppet pour des déploiements complexes et à grande échelle.

## Contexte
- Provisionner une infrastructure comprenant une instance EC2 de type t3.medium.

- Récupérer l'adresse IP de l'instance, l'ID, et la zone de disponibilité où se trouve l'instance, puis sauvegarder cela dans un fichier.

- Supprimer les ressources à l'aide de Terraform.

## Prérequis
[Configurer un compte AWS et installer Terraform.](https://gitlab.com/CarlinFongang-Labs/Terraform/lab0-sigup-aws)

[Réaliser un provionnement dynamique avec Terraform.](https://gitlab.com/CarlinFongang-Labs/Terraform/lab3-dynamic-deploy)

Obtenir la liste des AMI dans la région us-east-1 : 
````bash
aws ec2 describe-images --region us-east-1 --query 'Images[?Architecture==`x86_64`].[Name, ImageId]' --output text > image-aws-us-east-1.txt
````
On utulisera l'image Ubuntu 22.04 dont le nom est : **"ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20231207"**

## Ajout de l'autorisation de connexion ssh au vpc

Ce code crée une règle d'ingress pour autoriser le trafic SSH provenant de n'importe quelle adresse IP sur le port 22 dans un VPC.

````bash
  ingress {
    description = "SSH from VPC"
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
````

## Définition du remote provisionner

Ce code est un provisionnement à distance qui installe et configure le serveur web Nginx sur une machine distante. 
````bash
  provisioner "remote-exec" {
    inline = [ 
      "sleep 10",
      "sudo apt update -y",
      "sudo apt-get install nginx -y",
      "sudo systemctl start nginx",
    ]
  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = file("./secret/devops-aCD.pem")
    host = self.public_ip
    }
  }
````

## Définition du provisionner local
Doc sur l'utilisation des [Ressource aws_instance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance)

Ce provisionneur permettra de stocker des informations relative relatives à l'instance EC2 dans un fichier.
Le provisionner est ajouter dans le bloc de ressource eip, car l'ip ne pourra être récupéré et sauvegardé qu'après la création de l'élastic ip

````bash
  provisioner "local-exec" {
    command = "echo PUBLIC IP: ${self.public_ip}; ID: ${aws_instance.ec2-tf.id}; AZ: ${aws_instance.ec2-tf.availability_zone} > info_ec2-tf.txt"
  }
````

## Vérification de la conformité du code 
````bash
terraform validate
````

>![alt text](img/image.png)

## Exécution du provisionnement 
1. terraform plan 

>![alt text](img/image-1.png)
*Planification du provisionnement de 3 ressources*

2. terraform apply

Réussite du provisionnement et de la configuration de l'instance ec2
>![alt text](img/image-2.png)
*Succes du provisonnement*

3. Création du fichier info_ec2-tf.txt

Le fichier contenant les informations relatives à l'instance à bien été crée
>![alt text](img/image-3.png)

4. Instance provisionnée
>![alt text](img/image-4.png)

5. Vérification de la disponibilité du serveur nginx

Le serveur nginx est bien disponible via l'adresse eip crée et rattaché à l'instance
>![alt text](img/image-5.png)